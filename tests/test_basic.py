from unittest import mock

import pytest
from src import basic


@pytest.fixture
def operation_obj():
    return basic.Operation(num1=10, num2=20)


def test_add(operation_obj) -> None:
    assert operation_obj.add() == 30


def test_subtract(operation_obj) -> None:
    assert operation_obj.subtract() == -10


def test_multiply(operation_obj) -> None:
    assert operation_obj.multiply() == 200


@mock.patch("src.basic.Operation.random_add")
def test_random_add(mock_random_add, operation_obj) -> None:
    mock_random_add.return_value = 70
    assert operation_obj.random_add() == 70
