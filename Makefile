

python_install: #installing python version 3.8
	echo "============================Installing Python============================"
	sudo apt-get update
	sudo apt-get install python3.8
requirement_install: #installing libraries required for the current project
	echo "============================Installing Requirement============================"
	pip install pytest
	pip install pylint

run_test: #running unit test cases
	echo "============================Running Unit Test============================"
	echo "runniing test cases"
	pytest 

run_pylint: #running pylint to check the code
	echo "============================Running Pylint============================"
	pylint src/basic.py tests/test_basic.py

