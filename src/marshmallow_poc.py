from basic import Operation
from marshmallow import Schema, fields, post_load


class OperationSchema(Schema):
    num1 = fields.Integer()
    num2 = fields.Integer()

    @post_load
    def create_operation_object(self, data, **kwargs):
        return Operation(**data)


if __name__ == '__main__':
    input_dict = {
        'num1': 10,
        'num2': 20
    }

    schema = OperationSchema()
    operation = schema.load(input_dict)
    print(operation, type(operation))

    result = schema.dump(operation)
    print(result, type(result))
