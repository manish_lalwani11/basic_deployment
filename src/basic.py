import random


class Operation:
    def __init__(self, num1: int, num2: int):
        """
        :param num1: number1
        :param num2: number2
        """
        self.num1 = num1
        self.num2 = num2

    def add(self) -> int:
        """Function for adding 2 numbers"""
        return self.num1 + self.num2

    def subtract(self) -> int:
        """Function for subtraction"""
        return self.num1 - self.num2

    def multiply(self) -> int:
        """Function for multiplication"""
        return self.num1 * self.num2

    def random_add(self) -> int:
        """Adds any two random numbers created for mocking purpose"""
        return random.randint(10, 20) + random.randint(50, 100)
        # return self.num1 + self.num2


if __name__ == "__main__":
    op1 = Operation(num1=10, num2=20)
    print(op1)
    op1.add()
    op1.subtract()
    op1.multiply()
    op1.random_add()
