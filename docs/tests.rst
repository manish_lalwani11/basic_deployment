tests package
=============

Submodules
----------

tests.test\_basic module
------------------------

.. automodule:: tests.test_basic
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: tests
   :members:
   :undoc-members:
   :show-inheritance:
